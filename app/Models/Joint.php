<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Joint extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        "user_id",
        "team_id",
        "connection",
        "position",
        "x_coordinate",
        "y_coordinate",
        "type",
    ];
}
