<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'team_id', 'file_group_id', 'name',
        'path', 'extension', 'width', 'height', 'x_coordinate',
        'y_coordinate', 'z_index'
    ];
}
