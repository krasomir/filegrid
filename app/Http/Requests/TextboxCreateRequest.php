<?php

namespace App\Http\Requests;

use App\Models\Team;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TextboxCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'nullable|max:250',
            'team_id' => ['required', Rule::in(Team::all()->pluck('id'))],
            'user_id' => ['required', Rule::in(User::all()->pluck('id'))],
            'x_coordinate' => 'required|numeric',
            'y_coordinate' => 'required|numeric',
            'z_index' => 'required|numeric'
        ];
    }
}
