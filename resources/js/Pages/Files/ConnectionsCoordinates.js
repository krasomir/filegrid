// tpye: ['fileContainer', 'fence]

function getFirstX(connection, type) {
    let connectionEl = document.getElementById(type + connection.from)
    if (connectionEl) {
        if (type == 'fileContainer') {
            let fenceOffset = 0,
                fromFile = document.getElementById('fileContainer' + connection.from),
                fromParent = null
            if (fromFile.parentElement.id.includes('contentArea')) fromParent = fromFile.parentElement.id.slice(11)
            if (fromParent) fenceOffset += document.getElementById('fence' + fromParent).offsetLeft
            return connectionEl.offsetLeft + (connectionEl.offsetWidth / 2) + fenceOffset
        } else return connectionEl.offsetLeft + (connectionEl.offsetWidth / 2)
    }
}

function getFirstY(connection, type) {
    let connectionEl = document.getElementById(type + connection.from)
    if (connectionEl) {
        if (type == 'fileContainer') {
            let fenceOffset = 0,
                fromFile = document.getElementById('fileContainer' + connection.from),
                fromParent = null
            if (fromFile.parentElement.id.includes('contentArea')) fromParent = fromFile.parentElement.id.slice(11)
            if (fromParent) fenceOffset += (document.getElementById('fence' + fromParent).offsetTop + 44)
            return connectionEl.offsetTop + (connectionEl.offsetHeight / 2) + fenceOffset
        } else return connectionEl.offsetTop + (connectionEl.offsetHeight / 2)
    }
}

function getSecondX(connection, type) {
    let connectionEl = document.getElementById(type + connection.to)
    if (connectionEl) {
        if (type == 'fileContainer') {
            let fenceOffset = 0,
                toFile = document.getElementById('fileContainer' + connection.to),
                toParent = null
            if (toFile.parentElement.id.includes('contentArea')) toParent = toFile.parentElement.id.slice(11)
            if (toParent) fenceOffset += document.getElementById('fence' + toParent).offsetLeft
            return connectionEl.offsetLeft + (connectionEl.offsetWidth / 2) + fenceOffset
        } else return connectionEl.offsetLeft + (connectionEl.offsetWidth / 2)
    }
}

function getSecondY(connection, type) {
    let connectionEl = document.getElementById(type + connection.to)
    if (connectionEl) {
        if (type == 'fileContainer') {
            let fenceOffset = 0,
                toFile = document.getElementById('fileContainer' + connection.to),
                toParent = null
            if (toFile.parentElement.id.includes('contentArea')) toParent = toFile.parentElement.id.slice(11)
            if (toParent) fenceOffset += (document.getElementById('fence' + toParent).offsetTop + 44)
            return connectionEl.offsetTop + (connectionEl.offsetHeight / 2) + fenceOffset
        } else return connectionEl.offsetTop + (connectionEl.offsetHeight / 2)
    }
}

export { getFirstX, getFirstY, getSecondX, getSecondY }
