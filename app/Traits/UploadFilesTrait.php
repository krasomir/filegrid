<?php

namespace App\Traits;

use App\Models\FileUpload;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadFilesTrait
{
    /**
     * Undocumented function
     *
     * @param UploadedFile $file
     * @param String $folder
     * @param String $disk
     * @param String $encName
     * @return string|null
     */
    public static function upload(UploadedFile $file, String $folder, String $encName, String $disk)
    {
        $path = $file->storeAs(md5($folder), $encName, $disk);

        if ($file->isValid()) {
            return $path;
        }

        return null;
    }

    /**
     * Undocumented function
     *
     * @param String $path
     * @param String $disk
     * @return void
     */
    public static function delete(String $path, String $disk)
    {
        Storage::disk($disk)->delete($path);
    }
}
