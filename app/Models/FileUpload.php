<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileUpload extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable =  [
        'user_id',
        'team_id',
        'file_group_id',
        'name',
        'extension',
        'size',
        'path',
        'x_coordinate',
        'y_coordinate',
        'z_index',
    ];

    protected $with = ['user', 'team'];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    public function fileGroup(): BelongsTo
    {
        return $this->belongsTo(FileGroup::class);
    }
}
