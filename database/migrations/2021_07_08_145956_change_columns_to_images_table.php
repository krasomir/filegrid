<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsToImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->string('name')->nullable()->change();
            $table->string('path')->nullable()->change();
            $table->string('extension')->nullable()->change();
            $table->integer('width')->nullable()->change();
            $table->integer('height')->nullable()->change();
            $table->integer('x_coordinate')->nullable()->change();
            $table->integer('y_coordinate')->nullable()->change();
            $table->integer('z_index')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->string('name');
            $table->string('path');
            $table->string('extension');
            $table->integer('width');
            $table->integer('height');
            $table->integer('x_coordinate');
            $table->integer('y_coordinate');
            $table->integer('z_index');
        });
    }
}
