<?php

namespace App\Http\Requests;

use App\Models\Connection;
use App\Models\Team;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JointStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "user_id" => ["required", Rule::in(User::pluck('id'))],
            "team_id" => ["required", Rule::in(Team::pluck('id'))],
            "connection" => ["required", Rule::in(Connection::pluck('id'))],
            "position" => "required|numeric",
            "x_coordinate" => "required|numeric",
            "y_coordinate" => "required|numeric",
            "type" => ["required", Rule::in(Connection::$connectionTypes)],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            "user_id" => auth()->id(),
            "team_id" => auth()->user()->current_team_id,
        ]);
    }
}
