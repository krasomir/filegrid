<?php

namespace App\Http\Controllers;

use App\Events\TextboxCreateEvent;
use App\Events\TextboxDeleteEvent;
use App\Events\TextboxUpdateEvent;
use App\Models\Textbox;
use Illuminate\Http\Request;
use App\Http\Requests\TextboxCreateRequest;
use App\Http\Requests\TextboxUpdateRequest;

class TextboxController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TextboxCreateRequest $request)
    {
        try {

            $textbox = Textbox::create($request->validated());

            if ($textbox) {
                $textbox = $textbox->fresh();
                broadcast(new TextboxCreateEvent($textbox))->toOthers();

                return response()->json([
                    'error' => false,
                    'message' => 'Uspješno spremljeno.',
                    'textbox' => $textbox,
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom spremanja. ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TextboxUpdateRequest $request, Textbox $textbox)
    {
        if ($textbox->update($request->validated())) {

            $textbox = $textbox->fresh();
            broadcast(new TextboxUpdateEvent($textbox))->toOthers();

            return response()->json([
                'error' => false,
                'message' => 'Uspješno spremljeno.',
                'textbox' => $textbox,
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Greška prilikom spremanja.'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Textbox  $textbox
     * @return \Illuminate\Http\Response
     */
    public function destroy(Textbox $textbox)
    {
        if ($textbox->delete()) {

            broadcast(new TextboxDeleteEvent($textbox->id))->toOthers();

            return response()->json([
                'error' => false,
                'message' => 'Uspješno obrisano.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Greška prilikom brisanja.'
        ], 500);
    }
}
