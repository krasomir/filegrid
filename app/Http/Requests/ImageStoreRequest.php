<?php

namespace App\Http\Requests;

use App\Models\FileGroup;
use App\Models\Team;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ImageStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'x_coordinate' => ['required', 'numeric'],
            'y_coordinate' => ['required', 'numeric'],
            'file_group_id' => ['nullable', Rule::in(FileGroup::all()->pluck('id'))],
            'user_id' => ['required', Rule::in(User::all()->pluck('id'))],
            'team_id' => ['required', Rule::in(Team::all()->pluck('id'))],
            'file' => ['required', 'image', 'mimes:jpg,png,gif,jpeg,svg'],
        ];
    }
}
