// delete lines attached to deleted joint
const spliceHalfLines = (partLines, missingPosition) => {
    partLines.splice(partLines.findIndex(pl => pl.position === missingPosition), 2)
    // update positions of remaining partLines
    for (let i = 0; i < partLines.length; i++) {
        const line = partLines[i]
        if (line.position > missingPosition) line.position--
    }
}

// sort joints by position
const sortJoints = joints => joints.sort((prevJnt, nextJnt) => prevJnt.position - nextJnt.position)

// sort lines by position
const sortLines = lines => lines.sort((prevLine, nextLine) => prevLine.position - nextLine.position)

// find missing position
const findMissingPosition = joints => {
    let currentPosition = 1
    for (let i = 0; i < joints.length; i++) {
        const joint = joints[i]
        if (joint.position !== currentPosition++) return joint.position - 1
    }
    return currentPosition
}

export { spliceHalfLines, sortJoints, findMissingPosition, sortLines }
