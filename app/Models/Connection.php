<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Connection extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'team_id',
        'user_id',
        'from',
        'to',
        'color',
        'type',
    ];

    public static $connectionTypes = ['file_group', 'file', 'image'];

    public static function getAssociatedConnections(int $modelId): Collection
    {
        return self::where('from', $modelId)->orWhere('to', $modelId)->get();
    }

    public function joints(): HasMany
    {
        return $this->hasMany(Joint::class, 'connection');
    }

    protected static function booted(): void
    {
        static::deleting(function($connection) {
            $connection->joints()->delete();
        });
    }
}
