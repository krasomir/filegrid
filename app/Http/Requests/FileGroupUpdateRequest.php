<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileGroupUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'type' => 'in:fence,folder',
            'color' => 'regex:/^[#0-9A-Fa-f]{7}/',
            'width' => 'numeric',
            'height' => 'numeric',
            'z_index' => 'numeric',
            'x_coordinate' => 'numeric',
            'y_coordinate' => 'numeric'
        ];
    }
}
