<?php

namespace App\Http\Controllers;

use App\Events\ConnectionDeleteEvent;
use App\Events\CreateFileUploadEvent;
use Inertia\Inertia;
use Illuminate\Http\Request;

use App\Traits\UploadFilesTrait;

use App\Events\FileGridJoinEvent;
use App\Events\FileUploadDeleteEvent;
use App\Events\UpdateFileUploadEvent;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\FileUploadStoreRequest;
use App\Http\Requests\FileUploadUpdateRequest;
use Illuminate\Http\JsonResponse;
use App\Models\FileUpload;
use App\Models\FileGroup;
use App\Models\Connection;
use App\Models\Textbox;
use App\Models\Image;
use App\Models\FilesConnection;
use App\Models\Joint;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    use UploadFilesTrait;

    public function show(Request $request): \Inertia\Response
    {
        broadcast(new FileGridJoinEvent(auth()->user()))->toOthers();
        $connections = $this->getConnections();
        $joints = $this->getJoints();

        return Inertia::render('Files/Show', [
            'files' => FileUpload::where('team_id', auth()->user()->current_team_id)->get(),
            'fileGroups' => FileGroup::where('team_id', auth()->user()->current_team_id)->get(),
            'textboxes' => Textbox::where('team_id', $request->user()->current_team_id)->get(),
            'images' => Image::where('team_id', $request->user()->current_team_id)->get(),
            'connections' => $connections['fileGroupConnections'],
            'filesConnections' => $connections['fileConnections'],
            'imageConnections' => $connections['imageConnections'],
            'joints' => $joints['fileGroupJoints'],
            'fileJoints' => $joints['fileJoints'],
            'imageJoints' => $joints['imageJoints'],
        ]);
    }

    public function create(FileUploadStoreRequest $request): JsonResponse
    {
        try {
            if ($request->hasFile('file') && $request->validated()) {

                $file = $request->file('file');

                $fileUpload = FileUpload::create($request->except('file'));

                $encName = md5(time() . $file->getClientOriginalName());
                $path = self::upload($file, 'doc', $encName, config('filesystems.default'));

                if ($path) {
                    $fileUpload = $fileUpload->fresh();
                    $fileUpload->update([
                        'name' => $file->getClientOriginalName(),
                        'extension' => $file->extension(),
                        'size' => $file->getSize(),
                        'path' => $path,
                        'z_index' => 2,
                    ]);
                }

                broadcast(new CreateFileUploadEvent($fileUpload))->toOthers();

                return response()->json([
                    'error' => true,
                    'message' => 'Uspješno uploadano.',
                    'fileUpload' => $fileUpload,
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function update(FileUploadUpdateRequest $request, FileUpload $fileUpload): JsonResponse
    {
        if ($fileUpload->update($request->validated())) {

            $fileUpload = $fileUpload->fresh();
            broadcast(new UpdateFileUploadEvent($fileUpload))->toOthers();

            return response()->json([
                'error' => false,
                'message' => 'Uspješno ažurirano.',
                'fileUpload' => $fileUpload,
            ], 200);
        }
    }

    public function destroy(FileUpload $fileUpload): JsonResponse
    {
        try {
            return DB::transaction(function () use ($fileUpload) {
                if ($fileUpload->forceDelete()) {

                    if (Storage::disk(config('filesystems.default'))->exists($fileUpload->path)) {
                        self::delete($fileUpload->path, config('filesystems.default'));
                    }

                    $connections = Connection::getAssociatedConnections($fileUpload->id);
                    if ($connections->count() > 0) {
                        foreach ($connections as $connection) {
                            $connection->forceDelete();
                            broadcast(new ConnectionDeleteEvent($connection->id, $connection->type));
                        }
                    }

                    broadcast(new FileUploadDeleteEvent($fileUpload->id))->toOthers();

                    return response()->json([
                        'error' => false,
                        'message' => 'Uspješno obrisano.'
                    ], 200);
                }
            });
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom brisanja. '. $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Undocumented function
     *
     * @param FileUpload $fileUpload
     * @return Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadFile(FileUpload $fileUpload)
    {
        $url = Storage::url($fileUpload->path);

        if (Storage::missing($fileUpload->path)) {
            return response()->json([
                'message' => 'File not found.',
                'fileUrl' => $url,
            ], 404);
        }

        return Storage::download($fileUpload->path, $fileUpload->name);
    }

    public function getConnections(): array
    {
        $fileGroupConnections = collect();
        $fileConnections = collect();
        $imageConnections = collect();

        Connection::where('team_id', auth()->user()->current_team_id)
            ->get()
            ->filter(function($connection, $key) use ($fileGroupConnections, $fileConnections, $imageConnections) {
                switch ($connection->type) {
                    case 'file_group':
                        $fileGroupConnections->push($connection);
                        break;
                    case 'file':
                        $fileConnections->push($connection);
                        break;
                    case 'image':
                        $imageConnections->push($connection);
                        break;
                }
            });

        return [
            'fileGroupConnections' => $fileGroupConnections,
            'fileConnections' => $fileConnections,
            'imageConnections' => $imageConnections,
        ];
    }

    public function getJoints(): array
    {
        $fileGroupJoints = collect();
        $fileJoints = collect();
        $imageJoints = collect();

        Joint::where('team_id', auth()->user()->current_team_id)
            ->get()
            ->filter(function($joint, $key) use ($fileGroupJoints, $fileJoints, $imageJoints) {
                switch ($joint->type) {
                    case 'file_group':
                        $fileGroupJoints->push($joint);
                        break;
                    case 'file':
                        $fileJoints->push($joint);
                        break;
                    case 'image':
                        $imageJoints->push($joint);
                        break;
                }
            });

        return [
            'fileGroupJoints' => $fileGroupJoints,
            'fileJoints' => $fileJoints,
            'imageJoints' => $imageJoints
        ];
    }
}
