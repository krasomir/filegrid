<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNameAndColorToFileGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('file_groups', function (Blueprint $table) {
            $table->string('name')->nullable()->after('id');
            $table->string('color')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('file_groups', function (Blueprint $table) {
            $table->dropColumn(['name', 'color']);
        });
    }
}
