<?php

use App\Http\Controllers\ConnectionsController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;

use Inertia\Inertia;

use App\Http\Controllers\FileController;
use App\Http\Controllers\FileGroupsController;
use App\Http\Controllers\TextboxController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\FilesConnectionsController;
use App\Http\Controllers\JointController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::group(['middleware' => 'auth:sanctum', 'verified'], function () {
    // files
    Route::get('/files', [FileController::class, 'show'])->name('files.show');
    Route::post('/files/create', [FileController::class, 'create'])->name('files.create');
    Route::get('/files/{fileUpload}/download', [FileController::class, 'downloadFile'])->name('file.download');
    Route::patch('/files/{fileUpload}/update', [FileController::class, 'update'])->name('files.update');
    Route::delete('/files/{fileUpload}/delete', [FileController::class, 'destroy'])->name('files.delete');

    // file groups - fences and folders
    Route::post('/create-fence', [FileGroupsController::class, 'create']);
    Route::delete('/fence/{fileGroup}', [FileGroupsController::class, 'destroy']);
    Route::patch('/fence/{fileGroup}', [FileGroupsController::class, 'update']);

    // fence connections
    Route::post('/connections', [ConnectionsController::class, 'store']);
    Route::delete('/connections/{connection}', [ConnectionsController::class, 'destroy']);

    // Textboxes
    Route::post('/textboxes', [TextboxController::class, 'store']);
    Route::patch('/textboxes/{textbox}/update', [TextboxController::class, 'update']);
    Route::delete('/textboxes/{textbox}/delete', [TextboxController::class, 'destroy']);

    // Images
    Route::post('/images', [ImageController::class, 'store']);
    Route::patch('/images/{image}/update', [ImageController::class, 'update']);
    Route::delete('/images/{image}/delete', [ImageController::class, 'destroy']);

    // Joints
    Route::resource('joints', JointController::class)->only(['store', 'update', 'destroy']);

});
