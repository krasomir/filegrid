<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUploadStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'required|mimetypes:text/plain,application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/vnd.ms-powerpoint,application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'x_coordinate' => 'required|numeric',
            'y_coordinate' => 'required|numeric',
            'user_id' => 'required',
            'team_id' => 'required',
            'file_group_id' => 'nullable',
            'z_index' => 'nullable',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->id(),
            'team_id' => auth()->user()->current_team_id,
            'file_group_id' => ($this->file_group_id && $this->file_group_id != 'null') ? $this->file_group_id : null,
            'z_index' => $this->z_index ? $this->z_index : 0,
        ]);
    }
}
