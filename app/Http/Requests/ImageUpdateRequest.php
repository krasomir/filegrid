<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImageUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'x_coordinate' => 'nullable|numeric',
            'y_coordinate' => 'nullable|numeric',
            'file_group_id' => 'nullable|numeric',
            'height' => 'nullable|numeric',
            'width' => 'nullable|numeric'
        ];
    }
}
