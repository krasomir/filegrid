<?php

namespace App\Http\Controllers;

use App\Models\FileGroup;
use App\Models\Connection;
use App\Models\FileUpload;
use Illuminate\Http\Request;
use App\Traits\UploadFilesTrait;
use Illuminate\Http\JsonResponse;
use App\Events\CreateFileGroupEvent;
use App\Events\RemoveFileGroupEvent;
use App\Events\UpdateFileGroupEvent;
use App\Events\ConnectionDeleteEvent;
use App\Http\Requests\FileGroupStoreRequest;
use App\Http\Requests\FileGroupUpdateRequest;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class FileGroupsController extends Controller
{
    public function create(FileGroupStoreRequest $request): JsonResponse
    {
        try {

            $fileGroup = FileGroup::create($request->validated() + ['team_id' => auth()->user()->current_team_id]);

            if ($fileGroup) {

                $fileGroup = $fileGroup->fresh();
                broadcast(new CreateFileGroupEvent($fileGroup))->toOthers();

                return response()->json([
                    'error' => false,
                    'message' => 'Uspješno spremljeno.',
                    'fileGroup' => $fileGroup,
                ], 200);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom spremanja. ' . $e->getMessage(),
            ], 500);
        }
    }

    public function update(FileGroup $fileGroup, FileGroupUpdateRequest $request): JsonResponse
    {
        if ($fileGroup->update($request->validated())) {

            $fileGroup = $fileGroup->fresh();
            broadcast(new UpdateFileGroupEvent($fileGroup))->toOthers();

            return response()->json([
                'error' => false,
                'message' => 'Uspješno ažurirano.',
                'fileGroup' => $fileGroup,
            ], 200);
        }
    }

    public function destroy(FileGroup $fileGroup): JsonResponse
    {
        try {
            return DB::transaction(function () use ($fileGroup) {

                $allConnections = collect();
                $fileGroupConnections = Connection::getAssociatedConnections($fileGroup->id);
                $allConnections->push($fileGroupConnections);

                if ($fileGroup->delete()) {

                    foreach ($fileGroup->files as $file) {
                        $file->forceDelete();
                        UploadFilesTrait::delete($file->path, config('filesystems.default'));
                        $fileConnection = Connection::getAssociatedConnections($file->id);
                        $allConnections->push($fileConnection);
                    }

                    foreach ($fileGroup->images as  $image) {
                        $image->forceDelete();
                        UploadFilesTrait::delete($image->path, config('filesystems.default'));
                        // TODO add all associated image connections to $allConnections variable
                        // for delete and broadcast connections
                    }

                    $allConnections = $allConnections->flatten();
                    if ($allConnections->count() > 0) {
                        foreach ($allConnections as $connection) {
                            ($connection->type === 'file' || $connection->type === 'image')
                                ? $connection->forceDelete()
                                : $connection->delete();
                            broadcast(new ConnectionDeleteEvent($connection->id, $connection->type));
                        }
                    }

                    broadcast(new RemoveFileGroupEvent($fileGroup));

                    return response()->json([
                        'message' => 'Uspješno obrisano.',
                        'removedFileGroup' => $fileGroup,
                    ], 200);
                }
            });
        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Greška prilikom brisanja. ' . $e->getMessage()
            ], 500);
        }
    }
}
