<?php

namespace App\Http\Controllers;

use App\Models\Connection;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Events\ConnectionCreateEvent;
use App\Events\ConnectionDeleteEvent;
use App\Http\Requests\ConnectionStoreRequest;
use Illuminate\Support\Facades\DB;

class ConnectionsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ConnectionStoreRequest $request): JsonResponse
    {
        try {

            $connection = Connection::create($request->validated());

            if ($connection) {

                $connection = $connection->fresh();
                broadcast(new ConnectionCreateEvent($connection))->toOthers();

                return response()->json([
                    'error' => false,
                    'message' => 'Uspješno spremljeno.',
                    'connection' => $connection,
                ], 200);
            }

        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom spremanja. ' . $e->getMessage(),
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Connection $connection
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Connection $connection): JsonResponse
    {
        try {
            return DB::transaction(function () use ($connection) {
                if ($connection->delete()) {

                    broadcast(new ConnectionDeleteEvent($connection->id, $connection->type))->toOthers();

                    return response()->json([
                        'error' => false,
                        'message' => 'Uspješno obrisano.'
                    ], 200);
                }
            });
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom brisanja. '. $e->getMessage()
            ], 500);
        }
    }
}
