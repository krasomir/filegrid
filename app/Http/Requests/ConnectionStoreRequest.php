<?php

namespace App\Http\Requests;

use App\Models\Connection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConnectionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true: false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'team_id' => 'required|numeric',
            'user_id' => 'required|numeric',
            'from' => 'required|numeric',
            'to' => 'required|numeric',
            'color' => 'required|regex:/^[#0-9A-Fa-f]{7}/',
            'type' => ['required', Rule::in(Connection::$connectionTypes)],
        ];
    }
}
