<?php

namespace App\Http\Controllers;

use App\Events\JointCreateEvent;
use App\Events\JointDeleteEvent;
use App\Events\JointUpdateEvent;
use App\Http\Requests\JointStoreRequest;
use App\Http\Requests\JointUpdateRequest;
use App\Models\Joint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JointController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JointStoreRequest $request)
    {
        try {
            return DB::transaction(function () use ($request) {

                $existingJoints = Joint::where('connection', $request->connection)->get();
                if ($existingJoints) {
                    foreach ($existingJoints as $exsistingJoint) {
                        if ($exsistingJoint->position >= $request->position) {
                            $exsistingJoint->increment('position', 1);
                        }
                    }
                }
                $joint = Joint::create($request->validated());

                if ($joint) {
                    $newJoint = $joint->fresh();
                    broadcast(new JointCreateEvent($newJoint))->toOthers();
                    return response()->json([
                        'error' => false,
                        'message' => 'Uspješno spremljeno',
                        'joint' => $newJoint,
                    ], 200);
                }
            });
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom spremanja. ' . $e->getMessage()
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Joint $joint
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(JointUpdateRequest $request, Joint $joint)
    {
        if ($joint->update($request->validated())) {

            $updatedJoint = $joint->fresh();
            broadcast(new JointUpdateEvent($updatedJoint))->toOthers();
            return response()->json([
                'error' => false,
                'message' => 'Uspješno spremljena promjena.',
                'joint' => $updatedJoint,
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Greška prilikom spremanja promjena.'
        ], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Joint $joint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Joint $joint)
    {
        try {
            return DB::transaction(function () use ($joint) {

                if ($joint->delete()) {

                    $existingJoints = Joint::where('connection', $joint->connection)->get();
                    if ($existingJoints) {
                        foreach ($existingJoints as $exsistingJoint) {
                            if ($exsistingJoint->position > $joint->position) {
                                $exsistingJoint->decrement('position', 1);
                            }
                        }
                    }

                    broadcast(new JointDeleteEvent($joint))->toOthers();
                    return response()->json([
                        'error' => false,
                        'message' => 'Uspješno obrisano.'
                    ], 200);
                }
            });
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom brianja. '. $e->getMessage(),
            ], 500);
        }
    }
}
