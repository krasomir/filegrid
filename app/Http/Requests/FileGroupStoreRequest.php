<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileGroupStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required|in:fence,folder',
            'color' => 'required',
            'width' => 'required|numeric',
            'height' => 'required|numeric',
            'z_index' => 'required|numeric',
            'x_coordinate' => 'required|numeric',
            'y_coordinate' => 'required|numeric'
        ];
    }
}
