<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FileGroup extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable =  [
        'name',
        'team_id',
        'parent_id',
        'type',
        'color',
        'width',
        'height',
        'x_coordinate',
        'y_coordinate',
        'z_index',
    ];

    protected $with = ['team', 'files', 'images'];

    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    public function files(): HasMany
    {
        return $this->hasMany(FileUpload::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
