<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Textbox extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'textboxes';

    protected $fillable = [
        'content',
        'team_id',
        'user_id',
        'x_coordinate',
        'y_coordinate',
        'z_index',
    ];
}
