<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TextboxUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (auth()->check()) ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'nullable|max:250',
            'x_coordinate' => 'nullable|numeric',
            'y_coordinate' => 'nullable|numeric',
            'z_index' => 'nullable|numeric'
        ];
    }
}
