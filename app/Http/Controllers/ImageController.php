<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Events\ImageCreateEvent;
use App\Events\ImageDeleteEvent;
use App\Events\ImageUpdateEvent;
use App\Traits\UploadFilesTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ImageStoreRequest;
use App\Http\Requests\ImageUpdateRequest;

class ImageController extends Controller
{
    use UploadFilesTrait;

    public function store(ImageStoreRequest $request): JsonResponse
    {
        try {
            if ($request->hasFile('file') && $request->validated()) {

                $file = $request->file('file');
                $image = Image::create($request->except('file'));

                $encName = md5(time() . $file->getClientOriginalName());
                $path = self::upload($file, 'images', $encName, config('filesystems.default'));

                if ($path) {
                    $image = $image->fresh();
                    $image->update([
                        'name' => $file->getClientOriginalName(),
                        'extension' => $file->extension(),
                        'path' => $path,
                        'width' => 150,
                        'height' => 150,
                        'z_index' => 4,
                    ]);
                }

                broadcast(new ImageCreateEvent($image))->toOthers();

                return response()->json([
                    'error' => false,
                    'message' => 'Uspješno spremljeno.',
                    'image' => $image,
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom spremanja.' . $e->getMessage(),
            ], 500);
        }
    }

    public function update(ImageUpdateRequest $request, Image $image): JsonResponse
    {
        if ($image->update($request->validated())) {

            $image = $image->fresh();

            broadcast(new ImageUpdateEvent($image))->toOthers();

            return response()->json([
                'error' => false,
                'message' => 'Uspješno spremljeno.',
                'image' => $image,
            ], 200);
        }
        return response()->json([
            'error' => true,
            'message' => 'Greška prilikom spremanja.'
        ], 500);
    }

    public function destroy(Image $image): JsonResponse
    {
        try {
            $imagePath = $image->path;
            $imageId = $image->id;

            if ($image->forceDelete()) {

                if (Storage::disk(config('filesystems.default'))->exists($imagePath)) {
                    self::delete($imagePath, config('filesystems.default'));
                }

                broadcast(new ImageDeleteEvent($imageId))->toOthers();

                return response()->json([
                    'error' => false,
                    'message' => 'Uspješno obrisano.'
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'error' => true,
                'message' => 'Greška prilikom brisanja. '. $e->getMessage(),
            ], 500);
        }
    }
}
